# Example Polynomial Model Input File for AXISEM 2.0
NAME                iasp91

# is this model defined by multiple layers [layer] or polynomials [poly]
MODEL_TYPE          poly

# if anelastic, QMU an QKAPPA are defined, default: False
ANELASTIC           T

# reference frequency at which the velocities are defined in Hz, default: 1.
REFERENCE_FREQUENCY 1.

# if anisotropic, velocitities are defined as VPV, VPH, VSV, VSH and ETA is
# provided. If false, only VP and VS are needed. default: False
ANISOTROPIC         F

# number of regions in which the model is described by polynomials
NREGIONS            11

# radii of discontinuties, for whole planets starts from the center (0.0) and includes
# the free surface. This should hence be NREGIONS + 1 floats
DISCONTINUITIES     6371.0 6351.0 6336.0 6251.0 6161.0 5961.0 5711.0 5611.0 3631.0 3482.0 1217.0 0.0

# index of the Moho discontinuity from the top (0 = surface)
MOHO_IDX            2

# index of the discontinuity where Moho topography is compenstated from the top (0 = surface)
MOHO_COMP_IDX       3

# maximum polynomial degree throughout the whole model, default: 3
MAX_POLY_DEG        3

# prefactor to scale the radius before evaluating the polynomial, default:
# radius of the planet
SCALE               6371.0

# Are the units given in meters and meters per second?
# Allowed values:
#    m - lengths in m,  velocities in m/s,  density in kg/m^3
#   km - lengths in km, velocities in km/s, density in g/cm^3 
UNITS               km

# Indentations by at least two blanks are necessary.
# Assuming to have same order as DISCONTINUITIES (from center to the surface or vice versa)
RHO
  2.72
  2.92
  7.1586 -3.8601
  2.6910 0.6924 
  7.1089 -3.8045 
  5.3197 -1.4836 
  7.9565 -6.4761 5.5283 -3.0807 
  7.9565 -6.4761 5.5283 -3.0807 
  7.9565 -6.4761 5.5283 -3.0807 
  12.5815 -1.2638 -3.6426 -5.5281 
  13.0885 0.0 -8.8381 

VP
  5.8
  6.5
  8.78541 -0.74953 
  25.41389 -17.69722 
  30.78765 -23.25415 
  29.38896 -21.40656 
  25.96984 -16.93412 
  25.1486 -41.1538 51.9932 -26.6083 
  14.49470 -1.47089 
  10.03904 3.75665 -13.67046 
  11.24094 0.0 -4.09689 

# Fluid is detected by a single 0. value
VS
  3.36
  3.75
  6.706231 -2.248585 
  5.75020 -1.2742 
  15.24213 -11.08552 
  17.70732 -13.50652 
  20.76890 -16.53147 
  12.9303 -21.2590 27.8988 -14.1080 
  8.16616 -1.58206 
  0.0
  3.56454 0.0 -3.45241 

QMU
  600.0
  600.0
  600.0
  600.0
  143.0
  143.0
  312.0
  312.0
  312.0
  0.0
  84.6

QKAPPA
  57827.0
  57827.0
  57827.0
  57827.0
  57827.0
  57827.0
  57827.0
  57827.0
  57827.0
  57827.0
  1327.7
